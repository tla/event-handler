# event handler
cross-browser events handler for make js events user-friendly

# how to use

    var inst = new eventHandler();

listen an event

    inst.listen( element , event , function );

use capture

    inst.listen( element , event , function , true );

css selector filter

    inst.listen( element , event , css-selector , function );

use namespace

    inst.listen( element , "event.a.b.c" , function );

listen several events

    inst.listen( element , "event1.AND-OR.namespace , event2.AND-OR.namespace" , function );

    inst.listen( element [, css-selector ] , {

        "event1.AND-OR.namespace , event2.AND-OR.namespace" : function,

        "event3.AND-OR.namespace" : function

    } [, capture ] );

advanced settings

    inst.listen( element , events.AND-OR.namespace , function , {

        once : true / false,

        passive : true / false,

        capture : true / false,

        selector : '#X > .Y', // css selector filter

        blacklist : '#Y > .Z', // blacklist css selector

        autokill : function( settings /* { .capture , .selector , ... } */ ){

            // I'm called each times I'm triggered
            // if I'm return explicitely true, it will remove me

            if( someStuff ){
                return true;
            }

        },

        namespace:'...' // will be concat with namespaces events if defined

    });

 the event object

    inst.listen( element , event , function( e ){

        // -> the function argument isn't the native event object
        // -> its properties can't be rewrited
        // the original event object
        e.defaultEvent

        // element
        this
        e.element

        // event settings
        // { .capture , .selector , ... }
        e.setting

        // setting.selector element
        e.closest

        // remove the event
        e.mute()

        e.defaultPrevented // true / false
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()

        e...[ standard properties ]

    });

dispatch an event

-> **_if an event was listened with several namespaces, only one need to match for be triggered_**

    inst.dispatch( element , events.AND.namespace );
    
dispatch manualy (don't send a native event)

    inst.localDispatch( element , events.AND-OR.namespace );

    inst.localDispatch( element );

    inst.localDispatch( events.AND-OR.namespace );

shortcuts

    inst.click( element , function ); // listen
    ...

    inst.click( element ); // dispatch
    ...

remove an event

-> _**if an event was listened with several namespaces, only one need to match for be removed**_

    inst.mute( element , events.AND-OR.namespace , function );

    inst.mute( element , events.AND-OR.namespace );

    inst.mute( element );

    inst.mute( events.AND-OR.namespace );

remove all events from html block

    inst.muteChild( element );

make events passive by default

    inst.setDefaultPassive( true );

auto remove events when the element isn't in the DOM anymore

    inst.setLightMemoryMode( true );

kill the instance and all its events

    inst.destroy();

auto kill the instance

    event.setSelfDestruction( function( inst ){

        // I'm called if I haven't any listened events
        // if I'm return explicitely true, it will call the event.destroy function

        if( someStuff ){
            return true;
        }

    } );

how many events are listened

    inst.listened