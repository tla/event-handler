/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * event-handler
 * @see {@link https://framagit.org/tla/event-handler}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/event-handler/blob/master/LICENSE}
 *
 * @use js-tools-kit
 * @see {@link https://framagit.org/tla/js-tools-kit}
 * */
( function () {
    
    'use strict';
    
    var global ,
        hasProperty = Object.prototype.hasOwnProperty;
    
    try {
        global = Function( 'return this' )() || ( 42, eval )( 'this' );
    } catch ( e ) {
        global = window;
    }
    
    /**
     * js-tools-kit
     * @license MIT
     * @see {@link https://framagit.org/tla/js-tools-kit}
     * */
    ( function () {
        
        function _defineProperty ( obj , key , prop ) {
            Object.defineProperty( obj , key , {
                configurable : false ,
                enumerable : true ,
                writable : false ,
                value : prop
            } );
        }
        
        /**
         * @readonly
         * @property {Window} global
         * */
        _defineProperty( global , 'global' , global );
        
        /**
         * @readonly
         * @function _defineProperty
         * @param {Object} obj
         * @param {string} key
         * @param {*} prop
         * */
        _defineProperty( global , '_defineProperty' , _defineProperty );
        
        /**
         * @readonly
         * @function isset
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isset' , function ( _ ) {
            return _ !== undefined;
        } );
        
        /**
         * @readonly
         * @function isnull
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnull' , function ( _ ) {
            return _ === null;
        } );
        
        /**
         * @readonly
         * @function exist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'exist' , function ( _ ) {
            return isset( _ ) && !isnull( _ );
        } );
        
        /**
         * @readonly
         * @function isdate
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdate' , function ( _ ) {
            return exist( _ ) && _ instanceof Date;
        } );
        
        /**
         * @readonly
         * @function isarray
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isarray' , function ( _ ) {
            return exist( _ ) && Array.isArray( _ );
        } );
        
        /**
         * @readonly
         * @function isobject
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isobject' , function ( _ ) {
            return exist( _ ) &&
                _ instanceof Object && _.toString() === '[object Object]' &&
                _.constructor.hasOwnProperty( 'defineProperty' );
        } );
        
        /**
         * @readonly
         * @function isstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isstring' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'string' || _ instanceof global.String );
        } );
        
        /**
         * @readonly
         * @function isfillstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfillstring' , function ( _ ) {
            return isstring( _ ) && !!_.trim();
        } );
        
        /**
         * @readonly
         * @function isboolean
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isboolean' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'boolean' || _ instanceof global.Boolean );
        } );
        
        /**
         * @readonly
         * @function isinteger
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isinteger' , function ( _ ) {
            return exist( _ ) && Number.isInteger( _ );
        } );
        
        /**
         * @readonly
         * @function isfloat
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfloat' , function ( _ ) {
            return exist( _ ) && typeof _ == 'number' && isFinite( _ );
        } );
        
        /**
         * @readonly
         * @function isfunction
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfunction' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'function' || _ instanceof global.Function );
        } );
        
        /**
         * @readonly
         * @function isevent
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isevent' , function ( _ ) {
            if ( exist( _ ) ) {
                return ( _ instanceof global.Event ) ||
                    ( exist( _.defaultEvent ) && _.defaultEvent instanceof global.Event ) ||
                    ( exist( _.originalEvent ) && _.originalEvent instanceof global.Event );
            }
            return false;
        } );
        
        /**
         * @readonly
         * @function isregexp
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isregexp' , function ( _ ) {
            return exist( _ ) && ( _ instanceof global.RegExp );
        } );
        
        /**
         * @readonly
         * @function isnodelist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnodelist' , function ( _ ) {
            return exist( _ ) && (
                ( _ instanceof global.NodeList || _ instanceof global.HTMLCollection ) ||
                ( !isset( _.slice ) && isset( _.length ) && typeof _ === 'object' )
            );
        } );
        
        /**
         * @readonly
         * @function isdocument
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdocument' , function ( _ ) {
            return exist( _ ) && exist( _.defaultView ) && _ instanceof _.defaultView.HTMLDocument;
        } );
        
        /**
         * @readonly
         * @function iswindow
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'iswindow' , function ( _ ) {
            return exist( _ ) && ( _ === global || ( exist( _.window ) &&
                exist( _.window.constructor ) && _ instanceof _.window.constructor ) );
        } );
        
        /**
         * @readonly
         * @function getdocument
         * @param {*} _
         * @return {?HTMLDocument}
         * */
        _defineProperty( global , 'getdocument' , function ( _ ) {
            var tmp;
            
            if ( exist( _ ) ) {
                
                if ( isdocument( _ ) ) {
                    return _;
                }
                
                if ( isdocument( tmp = _.ownerDocument ) ) {
                    return tmp;
                }
                
                if ( iswindow( _ ) ) {
                    return _.document;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function getwindow
         * @param {*} _
         * @return {!Window}
         * */
        _defineProperty( global , 'getwindow' , function ( _ ) {
            var tmp;
            
            if ( iswindow( _ ) ) {
                return _;
            }
            
            if ( tmp = getdocument( _ ) ) {
                return tmp.defaultView;
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function isnode
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnode' , function ( _ ) {
            var doc , tag;
            
            if ( ( doc = getdocument( _ ) ) && isstring( tag = _.tagName ) ) {
                return ( doc.createElement( tag ) instanceof _.constructor ||
                    getdocument( global ).createElement( tag ) instanceof _.constructor );
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function isfragment
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfragment' , function ( _ ) {
            var win;
            
            return ( win = getwindow( _ ) ) &&
                ( _ instanceof win.DocumentFragment || _ instanceof global.DocumentFragment );
        } );
        
        /**
         * @readonly
         * @property {boolean} eventPassiveSupport
         * */
        ( function () {
            var tmp ,
                sup = false ,
                document = getdocument( global );
            
            if ( document ) {
                tmp = document.createDocumentFragment();
                
                var empty = function () {} , setting = {
                    get passive () {
                        sup = true;
                        return false;
                    }
                };
                
                tmp.addEventListener( 'click' , empty , setting );
            }
            
            _defineProperty( global , 'eventPassiveSupport' , sup );
        } )();
        
        /**
         * @readonly
         * @function domLoad
         * @param {function} callback
         * */
        _defineProperty( global , 'domLoad' , ( function () {
            var ready = false ,
                stackfunction = [] ,
                document = getdocument( global );
            
            function stack ( callback ) {
                if ( isfunction( callback ) ) {
                    if ( ready ) {
                        return callback.call( document );
                    }
                    
                    stackfunction.push( callback );
                }
            }
            
            function load () {
                if ( !ready ) {
                    ready = true;
                    
                    stackfunction.forEach( function ( callback ) {
                        callback.call( document );
                    } );
                    
                    stackfunction = null;
                    
                    if ( document ) {
                        document.removeEventListener( 'DOMContentLoaded' , load , true );
                        document.removeEventListener( 'DomContentLoaded' , load , true );
                        document.removeEventListener( 'load' , load , true );
                    }
                    
                    global.removeEventListener( 'load' , load , true );
                }
            }
            
            if ( document ) {
                document.addEventListener( 'DOMContentLoaded' , load , true );
                document.addEventListener( 'DomContentLoaded' , load , true );
                document.addEventListener( 'load' , load , true );
            }
            
            global.addEventListener( 'load' , load , true );
            
            return stack;
        } )() );
        
        /**
         * @readonly
         * @function matchesSelector
         * @param {HTMLElement|Element|Node} e - element
         * @param {string} s - css selector
         * @return {!boolean}
         * */
        _defineProperty( global , 'matchesSelector' , ( function () {
            var document = getdocument( global ) ,
                match = null ,
                tmp;
            
            if ( document ) {
                tmp = document.createElement( 'div' );
                
                match = tmp[ 'matches' ] && 'matches' ||
                    tmp[ 'matchesSelector' ] && 'matchesSelector' ||
                    tmp[ 'webkitMatchesSelector' ] && 'webkitMatchesSelector' ||
                    tmp[ 'mozMatchesSelector' ] && 'mozMatchesSelector' ||
                    tmp[ 'oMatchesSelector' ] && 'oMatchesSelector' ||
                    tmp[ 'msMatchesSelector' ] && 'msMatchesSelector' || null;
            }
            
            return function ( e , s ) {
                if ( !match || !e[ match ] ) {
                    return false;
                }
                
                return e[ match ]( s );
            };
        } )() );
        
        /**
         * @readonly
         * @function nl2br
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'nl2br' , function ( str ) {
            if ( isstring( str ) ) {
                return str
                    .replace( /\n/g , "<br/>" )
                    .replace( /\t/g , "&nbsp;&nbsp;&nbsp;&nbsp;" );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function inarray
         * @param {*} _in
         * @param {Array} array
         * @return {!boolean}
         * */
        _defineProperty( global , 'inarray' , function ( _in , array ) {
            return array.indexOf( _in ) >= 0;
        } );
        
        /**
         * @readonly
         * @function arrayunique
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'arrayunique' , function ( _ ) {
            for ( var n = [] , i = 0 , l = _.length ; i < l ; i++ ) {
                if ( !inarray( _[ i ] , n ) ) {
                    n.push( _[ i ] );
                }
            }
            return n;
        } );
        
        /**
         * @readonly
         * @function typedtoarray
         * @param {*} arg
         * @return {!Array}
         * */
        _defineProperty( global , 'typedtoarray' , function ( arg ) {
            return Array.apply( null , arg );
        } );
        
        /**
         * @readonly
         * @function fragmenttoarray
         * @param {DocumentFragment} frag
         * @return {!Array}
         * */
        _defineProperty( global , 'fragmenttoarray' , function ( frag ) {
            return typedtoarray( frag.children );
        } );
        
        /**
         * @readonly
         * @function getallchildren
         * @param {HTMLElement|Element|Node} element
         * @return {!NodeList}
         * */
        _defineProperty( global , 'getallchildren' , function ( element ) {
            return element.getElementsByTagName( '*' );
        } );
        
        /**
         * @readonly
         * @function emptyNode
         * @param {HTMLElement|Element|Node} element
         * @return {!(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'emptyNode' , function ( element ) {
            var c;
            
            while ( c = element.firstChild ) {
                element.removeChild( c );
            }
            
            return element;
        } );
        
        /**
         * @readonly
         * @function closest
         * @param {HTMLElement|Element|Node} element
         * @param {string} selector
         * @return {?(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'closest' , ( function () {
            var document = getdocument( global );
            
            if ( document && isfunction( document.createElement( 'div' ).closest ) ) {
                return function ( element , selector ) {
                    if ( !isnode( element ) ) {
                        return null;
                    }
                    
                    return element.closest( selector );
                };
            }
            
            return function ( element , selector ) {
                if ( !isnode( element ) ) {
                    return null;
                }
                
                if ( matchesSelector( element , selector ) ) {
                    return element;
                }
                
                while ( isnode( element = element.parentNode ) ) {
                    if ( matchesSelector( element , selector ) ) {
                        return element;
                    }
                }
                
                return null;
            };
        } )() );
        
        /**
         * @readonly
         * @function randomstring
         * @return {!string}
         * */
        _defineProperty( global , 'randomstring' , function () {
            return '_' + Math.random().toString( 30 ).substring( 2 );
        } );
        
        /**
         * @readonly
         * @function between
         * @param {number} min
         * @param {number} max
         * @return {!number}
         * */
        _defineProperty( global , 'between' , function ( min , max ) {
            return Math.floor( Math.random() * ( max - min + 1 ) + min );
        } );
        
        /**
         * @readonly
         * @function round
         * @param {number} int
         * @param {number} after
         * @return {!number}
         * */
        _defineProperty( global , 'round' , function ( int , after ) {
            return parseFloat( int.toFixed( isinteger( after ) ? after : int.toString().length ) );
        } );
        
        /**
         * @readonly
         * @function wrapint
         * @param {number} int
         * @param {number} howmany
         * @return {!number}
         * */
        _defineProperty( global , 'wrapinteger' , function ( int , howmany ) {
            int = int.toString();
            
            while ( int.length < howmany ) {
                int = '0' + int;
            }
            
            return int;
        } );
        
        /**
         * @readonly
         * @function jsonparse
         * @param {string} str
         * @return {?Object}
         * */
        _defineProperty( global , 'jsonparse' , function ( str ) {
            var result = null ,
                masterKey;
            
            try {
                result = JSON.parse( str );
            } catch ( e ) {
                try {
                    masterKey = randomstring();
                    
                    global[ masterKey ] = null;
                    
                    eval( 'global[ masterKey ] = ' + str );
                    
                    if ( isobject( global[ masterKey ] ) ) {
                        result = global[ masterKey ] || null;
                    }
                } catch ( e ) {
                    result = null;
                } finally {
                    delete global[ masterKey ];
                }
            }
            
            return result;
        } );
        
        if ( !isfunction( global.setImmediate ) ) {
            ( function () {
                var message = 0 ,
                    register = {} ,
                    prefix = randomstring() + '.immediate';
                
                function getLocation ( location ) {
                    try {
                        if ( isfillstring( location.origin ) && location.origin !== 'null' ) {
                            return location.origin;
                        }
                        
                        if ( location.ancestorOrigins && location.ancestorOrigins.length ) {
                            return location.ancestorOrigins[ 0 ];
                        }
                    } catch ( _ ) {
                        return null;
                    }
                }
                
                var origin = ( function () {
                    var init = global;
                    
                    while ( true ) {
                        var tmp = getLocation( init.location );
                        
                        if ( tmp ) {
                            return tmp;
                        }
                        
                        if ( iswindow( init.parent ) ) {
                            init = init.parent;
                            continue;
                        }
                        
                        return '';
                    }
                } )();
                
                try {
                    global.postMessage( prefix + 1 , function () {} );
                } catch ( e ) {
                    return ( function () {
                        _defineProperty( global , 'setImmediate' , global.setTimeout );
                        _defineProperty( global , 'clearImmediate' , global.clearTimeout );
                    } )();
                }
                
                global.addEventListener( 'message' , function ( event ) {
                    if ( origin === event.origin ) {
                        var i = event.data;
                        
                        if ( isfunction( register[ i ] ) ) {
                            register[ i ]();
                            delete register[ i ];
                            event.stopImmediatePropagation();
                        }
                    }
                } , eventPassiveSupport ? { capture : true , passive : true } : true );
                
                /**
                 * @readonly
                 * @function setImmediate
                 * @param {function} fn
                 * @return {!number}
                 * */
                _defineProperty( global , 'setImmediate' , function ( fn ) {
                    var id = ++message;
                    
                    register[ prefix + id ] = fn;
                    global.postMessage( prefix + id , origin );
                    
                    return id;
                } );
                
                /**
                 * @readonly
                 * @function clearImmediate
                 * @param {number} id
                 * @return void
                 * */
                _defineProperty( global , 'clearImmediate' , function ( id ) {
                    if ( register[ prefix + id ] ) {
                        delete register[ prefix + id ];
                    }
                } );
            } )();
        }
        
        /**
         * @readonly
         * @function getstyleproperty
         * @param {HTMLElement|Element|Node} element
         * @param {string} property
         * @return {?string}
         * */
        _defineProperty( global , 'getstyleproperty' , function ( element , property ) {
            var val , win;
            
            if ( isnode( element ) ) {
                
                if ( val = element.style[ property ] ) {
                    return val;
                }
                
                if ( val = element.style.getPropertyValue( property ) ) {
                    return val;
                }
                
                if ( win = getwindow( element ) ) {
                    return win.getComputedStyle( element ).getPropertyValue( property ) || null;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function splitTrim
         * @param {string} str
         * @param {RegExp|string} spl
         * @return {Array}
         * */
        _defineProperty( global , 'splitTrim' , function ( str , spl ) {
            var ar = str.split( spl );
            
            for ( var i = 0 , r = [] , tmp ; i < ar.length ; ) {
                if ( tmp = ar[ i++ ].trim() ) {
                    r.push( tmp );
                }
            }
            
            return r;
        } );
        
        /**
         * @readonly
         * @function clonearray
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'clonearray' , function ( _ ) {
            return _.slice( 0 );
        } );
        
        /**
         * @readonly
         * @function toarray
         * @param {*} _
         * @return {!Array}
         * */
        _defineProperty( global , 'toarray' , function ( _ ) {
            return isset( _ ) ? ( isarray( _ ) ? _ : [ _ ] ) : [];
        } );
        
        /**
         * @readonly
         * @function counterCallback
         * @param {number} counter
         * @param {function} callback
         * @return {!Array}
         * */
        _defineProperty( global , 'counterCallback' , function ( counter , callback ) {
            var current = 0;
            
            return function () {
                if ( ++current == counter ) {
                    callback();
                }
            };
        } );
        
        /**
         * @readonly
         * @function regexpEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'regexpEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /\\/gi , '\\\\' )
                    .replace( /([.\[\](){}^?*|+\-$])/gi , '\\$1' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function htmlEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'htmlEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /&/gi , '&amp;' )
                    .replace( /"/gi , '&quot;' )
                    .replace( /'/gi , '&apos;' )
                    .replace( /</gi , '&lt;' )
                    .replace( />/gi , '&gt;' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function resetregexp
         * @param {RegExp} _
         * @return {?RegExp}
         * */
        _defineProperty( global , 'resetregexp' , function ( _ ) {
            if ( !isregexp( _ ) ) {
                return null;
            }
            
            return _.lastIndex = _.index = 0, _;
        } );
        
        /**
         * @readonly
         * @function overrideObject
         * @return {!Object}
         * */
        _defineProperty( global , 'overrideObject' , function () {
            var result = {} ,
                array = typedtoarray( arguments );
            
            if ( array.length <= 0 ) {
                return result;
            }
            
            if ( array.length == 1 ) {
                return isobject( array[ 0 ] ) ? array[ 0 ] : result;
            }
            
            for ( var i = 0 , l = array.length ; i < l ; i++ ) {
                forin( array[ i ] , function ( key , value ) {
                    result[ key ] = value;
                } );
            }
            
            return result;
        } );
        
        /**
         * @readonly
         * @function isEmptyObject
         * @param {Object} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isEmptyObject' , function ( _ ) {
            if ( isobject( _ ) ) {
                for ( var key in _ ) {
                    if ( hasProperty.call( _ , key ) ) {
                        return false;
                    }
                }
                
                return true;
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function forin
         * @param {Object} obj
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'forin' , function ( obj , callback ) {
            var key , value;
            
            if ( isobject( obj ) ) {
                for ( key in obj ) {
                    if ( hasProperty.call( obj , key ) && isset( value = obj[ key ] ) ) {
                        callback( key , value , obj );
                    }
                }
            }
        } );
        
        /**
         * @readonly
         * @function foreach
         * @param {Array} arr
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'foreach' , function ( arr , callback ) {
            if ( isarray( arr ) ) {
                for ( var i = 0 , l = arr.length ; i < l ; i++ ) {
                    callback( arr[ i ] , i , arr );
                }
            }
        } );
        
        /**
         * @readonly
         * @function for
         * @param {Array} arg
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'for' , function ( arg , callback ) {
            if ( isarray( arg ) ) {
                foreach( arg , callback );
            }
            else if ( isobject( arg ) ) {
                forin( arg , callback );
            }
        } );
        
        /**
         * @readonly
         * @function smoothyIncrement
         * @param {Object} setting
         * @param {number} setting.begin
         * @param {number} setting.end
         * @param {function} setting.eachFrame
         * @param {function} [setting.onFinish]
         * @param {number} [setting.speed=500]
         * @return void
         * */
        _defineProperty( global , 'smoothyIncrement' , ( function () {
            
            var frameLatency = 1000 / 60;
            
            var requestFrame = global[ 'requestAnimationFrame' ] ||
                global[ 'webkitRequestAnimationFrame' ] ||
                global[ 'mozRequestAnimationFrame' ] ||
                global[ 'msRequestAnimationFrame' ];
            
            var cancelFrame = global[ 'cancelAnimationFrame' ] ||
                global[ 'webkitCancelAnimationFrame' ] ||
                global[ 'mozCancelAnimationFrame' ] ||
                global[ 'msCancelAnimationFrame' ];
            
            function ease ( n ) {
                return 0.5 * ( 1 - Math.cos( Math.PI * n ) );
            }
            
            function animate ( callframe ) {
                var frame , start;
                
                function loop () {
                    frame = requestFrame( loop );
                    callframe();
                }
                
                start = setImmediate( loop );
                
                return function () {
                    clearImmediate( start );
                    cancelFrame( frame );
                };
            }
            
            return function ( setting ) {
                
                setting = overrideObject( {
                    speed : 1000
                } , setting );
                
                var begin = setting.begin ,
                    end = setting.end ,
                    speed = setting.speed ,
                    callback = setting.eachFrame ,
                    callbackFinish = setting.onFinish;
                
                if ( !isinteger( speed ) || speed <= 0 ) {
                    speed = 1000;
                }
                
                speed = Math.round( speed / 1.8 );
                
                var tmp = begin;
                begin = Math.min( begin , end );
                end = Math.max( tmp , end );
                
                var newval , elapsed;
                
                var firstStep = true ,
                    lastStep = false ,
                    endAtNext = false;
                
                var beginValues = [] ,
                    beginTotal = 0 ,
                    total = 0;
                
                var elapsedMax = 1 ,
                    elapsedMin = 0.1;
                
                var totalScroll = end - begin ,
                    increment = Math.round( totalScroll / ( speed / frameLatency ) );
                
                if ( totalScroll <= increment ) {
                    return callback( end , end - begin );
                }
                
                var startTime = Date.now();
                
                var stop = animate( function () {
                    
                    /* stop process */
                    /* ---------------------- */
                    if ( endAtNext ) {
                        callback( end , end - begin );
                        
                        if ( isfunction( callbackFinish ) ) {
                            callbackFinish();
                        }
                        
                        return stop();
                    }
                    
                    /* calc process */
                    /* ---------------------- */
                    
                    elapsed = round( ( Date.now() - startTime ) / speed , 2 );
                    
                    if ( elapsed < elapsedMin ) {
                        elapsed = elapsedMin;
                    }
                    else if ( elapsed > elapsedMax ) {
                        elapsed = elapsedMax;
                    }
                    
                    if ( firstStep && elapsed == elapsedMax ) {
                        firstStep = false;
                    }
                    
                    /* begin process */
                    /* ---------------------- */
                    
                    newval = Math.round( increment * ease( elapsed ) );
                    
                    if ( newval < 1 ) {
                        newval = 1;
                    }
                    
                    /* middle process */
                    /* ---------------------- */
                    
                    if ( firstStep ) {
                        beginValues.push( newval );
                        beginTotal += newval;
                    }
                    
                    if ( !firstStep && !lastStep && ( totalScroll - total ) <= beginTotal ) {
                        lastStep = true;
                    }
                    
                    /* end process */
                    /* ---------------------- */
                    
                    if ( lastStep ) {
                        if ( beginValues.length ) {
                            newval = beginValues.pop();
                        }
                        else {
                            newval = 1;
                        }
                    }
                    
                    total += newval;
                    
                    /* request stop process */
                    /* ---------------------- */
                    
                    if ( lastStep && totalScroll - total <= newval ) {
                        endAtNext = true;
                    }
                    
                    /* callback */
                    /* ---------------------- */
                    
                    callback( begin += newval , newval );
                    
                } );
                
                return stop;
                
            };
            
        } )() );
        
        /**
         * @readonly
         * @function cacheHandler
         * @param {number} [n=500]
         * @return {!function}
         * */
        _defineProperty( global , 'cacheHandler' , function ( n ) {
            var c = [];
            !n && ( n = 500 );
            
            function SAVE ( k , v ) {
                if ( !/^_(rm|purge)$/g.test( k ) ) {
                    SAVE[ k ] = v;
                    c.push( k );
                    
                    if ( c.length > n ) {
                        delete SAVE[ c.shift() ];
                    }
                }
            }
            
            SAVE._rm = function ( k ) {
                var i;
                
                if ( ( i = c.indexOf( k ), i ) !== -1 ) {
                    delete SAVE[ k ];
                    c.splice( i , 1 );
                }
            };
            
            SAVE._purge = function () {
                var i = c.length;
                
                while ( i-- ) {
                    delete SAVE[ c.shift() ];
                }
            };
            
            return SAVE;
        } );
        
        /**
         * @readonly
         * @function requireJS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireJS' , ( function () {
            var doc = getdocument( global );
            
            function require ( src , callback , document ) {
                var script;
                
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && script.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'script[src="' + src + '"]' ) ) {
                        return load();
                    }
                    
                    script = document.createElement( 'script' );
                    
                    script.setAttribute( 'src' , src );
                    script.setAttribute( 'async' , 'async' );
                    script.setAttribute( 'type' , 'text/javascript' );
                    
                    script.addEventListener( 'load' , load );
                    
                    document.head.appendChild( script );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function requireCSS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireCSS' , ( function () {
            var doc = getdocument( global );
            
            function require ( href , callback , document ) {
                var link;
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && link.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'link[href="' + href + '"]' ) ) {
                        return load();
                    }
                    
                    link = document.createElement( 'link' );
                    
                    link.setAttribute( 'href' , href );
                    link.setAttribute( 'rel' , 'stylesheet' );
                    
                    link.addEventListener( 'load' , load );
                    
                    document.head.appendChild( link );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function htmlDOM
         * @param {string} str
         * @return {?(HTMLElement|Element|Node|array)}
         * */
        _defineProperty( global , 'htmlDOM' , ( function () {
            var document = getdocument( global );
            
            if ( !document ) {
                return function () {
                    return null;
                };
            }
            
            var parentOf = {
                "col" : "colgroup" ,
                "tr" : "tbody" ,
                "th" : "tr" ,
                "td" : "tr" ,
                "colgroup" : "table" ,
                "tbody" : "table" ,
                "thead" : "table" ,
                "tfoot" : "table" ,
                "dt" : "dl" ,
                "dd" : "dl" ,
                "figcaption" : "figure" ,
                "legend" : "fieldset" ,
                "fieldset" : "form" ,
                "keygen" : "form" ,
                "area" : "map" ,
                "menuitem" : "menu" ,
                "li" : "ul" ,
                "option" : "optgroup" ,
                "optgroup" : "select" ,
                "output" : "form" ,
                "rt" : "ruby" ,
                "rp" : "ruby" ,
                "summary" : "details" ,
                "track" : "video" ,
                "source" : "video" ,
                "param" : "object"
            };
            
            var _ = cacheHandler();
            
            function clone ( e ) {
                if ( isarray( e ) ) {
                    for ( var i = 0 , l = e.length , r = [] ; i < l ; i++ ) {
                        r.push( e[ i ].cloneNode( true ) );
                    }
                    return r;
                }
                
                if ( exist( e ) ) {
                    return e.cloneNode( true );
                }
                
                return null;
            }
            
            function child ( e ) {
                return e.children.length == 1 ? e.children[ 0 ] : Array.apply( null , e.children );
            }
            
            function createHTML ( str ) {
                var firstTag = ( /^(<[\s]*[\w]{1,15}[\s]*(\b|>)?)/gi.exec( str ) || [ '' ] )[ 0 ]
                    .replace( /\W/g , '' ).trim().toLowerCase() ,
                    parent , doc;
                
                if ( !firstTag ) {
                    return;
                }
                
                if ( !parentOf[ firstTag ] ) {
                    doc = document.createDocumentFragment();
                    parent = document.createElement( 'DIV' );
                    doc.appendChild( parent );
                    parent.innerHTML = str;
                    return child( parent );
                }
                
                /** @type {Node} */
                parent = createHTML( '<' + parentOf[ firstTag ] + '></' + parentOf[ firstTag ] + '>' );
                
                while ( parent.firstChild ) {
                    parent = parent.firstChild;
                }
                
                parent.innerHTML = str;
                
                return child( parent );
            }
            
            return function ( str ) {
                var r;
                
                str = str.trim();
                
                if ( _[ str ] ) {
                    return clone( _[ str ] );
                }
                
                if ( !/(<[\s]*(\/)?[\s]*[\w]{1,15}[\s]*(\/)?[\s]*(\b|>)?)/gi.test( str ) ) {
                    return null;
                }
                
                if ( /^(<\/[\s]*[\w]{1,15}[\s]*>)$/gi.test( str ) ) {
                    r = document.createElement( str.replace( /\W/g , '' ).toUpperCase() );
                }
                else {
                    r = createHTML( str );
                }
                
                return _( str , r ) , clone( r );
            };
        } )() );
        
    } )();
    
    var guid = 0 ,
        trimCache = cacheHandler( 1000 ) ,
        concat = Array.prototype.push;
    
    function createEventObject ( window , name ) {
        try {
            return new ( window.CustomEvent )( name , {
                cancelable : true ,
                bubbles : true
            } );
        } catch ( e ) {
            return null;
        }
    }
    
    var dispatch = ( function () {
        
        function dispatch ( node , event ) {
            if ( node.dispatchEvent ) {
                node.dispatchEvent( event );
            }
        }
        
        return function ( node , eventName , data , emulate ) {
            var event;
            
            if ( eventName instanceof Event ) {
                return dispatch( node , eventName );
            }
            
            // native event ( without data... )
            if ( ( !exist( data ) || !data.length ) && !emulate && isfunction( node[ eventName ] ) ) {
                return node[ eventName ]();
            }
            
            // custom event
            if ( event = createEventObject( getwindow( node ) , eventName ) ) {
                event.eventHandler = data || null;
                event.manualyDispatched = true;
                dispatch( node , event );
            }
        };
        
    } )();
    
    function commat_splitTrim ( ar ) {
        return splitTrim( ar , /,|\s/g );
    }
    
    function dot_splitTrim ( ar ) {
        return splitTrim( ar , '.' );
    }
    
    function parseNamespace ( str , def ) {
        if ( str = ( str || '' ).toString().trim() ) {
            str = dot_splitTrim( str );
            
            if ( str.length ) {
                
                if ( def && def.length ) {
                    concat.apply( str , def );
                }
                
                return arrayunique( str ).sort();
                
            }
        }
        
        return null;
    }
    
    function compareNamespace ( ar1 , ar2 ) {
        var check = false;
        
        if ( ar1 && ar1.length && ar2 && ar2.length ) {
            for ( var i = 0 , l = ar1.length ; i < l ; i++ ) {
                if ( inarray( ar1[ i ] , ar2 ) ) {
                    check = true;
                    break;
                }
            }
        }
        
        return check;
    }
    
    function parseEventsArguments ( event , defNamespace ) {
        var i;
        return ( i = event.indexOf( '.' ), i ) < 0 ? {
                event : event || null ,
                namespace : null
            } :
            {
                event : event.slice( 0 , i ) || null ,
                namespace : parseNamespace( event.slice( i + 1 ) , defNamespace )
            };
    }
    
    function buildEvents ( events , defNamespace ) {
        var t , e = events;
        
        if ( t = trimCache[ e ] ) {
            return t;
        }
        
        events = commat_splitTrim( events );
        
        for ( var i = 0 , r = [] , l = events.length ; i < l ; ) {
            r.push( parseEventsArguments( events[ i++ ] , defNamespace ) );
        }
        
        return trimCache( e , r ), r;
    }
    
    function sortNamespace ( events ) {
        var namespaces = {
            global : []
        };
        
        if ( events ) {
            events.forEach( function ( model ) {
                if ( model.event ) {
                    if ( !namespaces[ model.event ] ) {
                        namespaces[ model.event ] = [];
                    }
                    
                    if ( model.namespace ) {
                        concat.apply( namespaces[ model.event ] , model.namespace );
                    }
                }
                else {
                    if ( model.namespace ) {
                        concat.apply( namespaces.global , model.namespace );
                    }
                }
            } );
        }
        
        return function ( event ) {
            var r = namespaces.global.slice( 0 );
            
            if ( namespaces[ event ] ) {
                concat.apply( r , namespaces[ event ] );
            }
            
            return r;
        };
    }
    
    var cloneSettings = ( function () {
        var prop = [ 'capture' , 'namespace' , 'once' , 'passive' , 'selector' , 'blacklist' ] , l = prop.length;
        
        return function ( origin ) {
            for ( var setting = {} , i = 0 , tmp ; i < l ; i++ ) {
                tmp = prop[ i ];
                setting[ tmp ] = origin[ tmp ];
            }
            return setting;
        };
    } )();
    
    var cloneEvent = ( function () {
        
        function setReadOnly ( obj , prop , val ) {
            Object.defineProperty( obj , prop , {
                configurable : false ,
                enumerable : true ,
                writable : false ,
                value : val
            } );
        }
        
        /** @typedef {!cevent#} EventClone */
        var cevent = function () {};
        
        [
            'altKey' , 'bubbles' , 'button' , 'buttons' ,
            'cancelable' , 'changedTouches' , 'clientX' , 'clientY' ,
            'clipboardData' , 'ctrlKey' , 'currentTarget' , 'composed' ,
            'data' , 'dataTransfer' , 'deltaX' , 'deltaY' , 'deltaZ' ,
            'deltaMode' , 'detail' , 'eventPhase' , 'fromElement' ,
            'getModifierState' , 'isComposing' , 'isTrusted' , 'key' ,
            'keyCode' , 'layerX' , 'layerY' , 'location' , 'metaKey' ,
            'movementX' , 'movementY' , 'offsetX' , 'offsetY' , 'pageX' ,
            'pageY' , 'relatedTarget' , 'region' , 'repeat' , 'screenX' ,
            'screenY' , 'shiftKey' , 'target' , 'targetTouches' , 'timeStamp' ,
            'toElement' , 'touches' , 'type' , 'view' , 'wheelDelta' ,
            'wheelDeltaX' , 'wheelDeltaY' , 'which' , 'x' , 'y' , 'manualyDispatched'
        ].forEach( function ( prop ) {
            Object.defineProperty( cevent.prototype , prop , {
                get : function () {
                    return this.defaultEvent[ prop ];
                }
            } );
        } );
        
        /**
         * @public
         * @readonly
         * @function mute
         * @memberof EventClone
         * */
        cevent.prototype.mute = function () {};
        
        /**
         * @public
         * @readonly
         * @function stopPropagation
         * @memberof EventClone
         * */
        cevent.prototype.stopPropagation = function () {
            this.defaultEvent.stopPropagation();
        };
        
        /**
         * @public
         * @readonly
         * @function stopImmediatePropagation
         * @memberof EventClone
         * */
        cevent.prototype.stopImmediatePropagation = function () {
            this.defaultEvent.stopImmediatePropagation();
        };
        
        /**
         * @public
         * @readonly
         * @function preventDefault
         * @memberof EventClone
         * */
        cevent.prototype.preventDefault = function () {
            if ( this.defaultEvent.cancelable && !this.defaultPrevented ) {
                this.defaultEvent.preventDefault();
            }
        };
        
        /**
         * @public
         * @readonly
         * @function getPreventDefault
         * @memberof EventClone
         * */
        cevent.prototype.getPreventDefault = function () {
            return this.defaultPrevented;
        };
        
        /**
         * @public
         * @readonly
         * @function isDefaultPrevented
         * @memberof EventClone
         * */
        cevent.prototype.isDefaultPrevented = function () {
            return this.defaultPrevented;
        };
        
        /**
         * @public
         * @readonly
         * @property {boolean} defaultPrevented
         * @memberof EventClone
         * */
        Object.defineProperty( cevent.prototype , 'defaultPrevented' , {
            get : function () {
                var e = this.defaultEvent;
                
                return isset( e.defaultPrevented ) ? e.defaultPrevented :
                    isset( e.getPreventDefault ) ? e.getPreventDefault() : false;
            }
        } );
        
        /**
         * @function cloneEvent
         * @return {cevent}
         * */
        return function ( event , setting , closest , element , override ) {
            
            /** @type {EventClone} */
            var a = new cevent();
            
            /**
             * @public
             * @readonly
             * @property {Object} setting
             * @memberof EventClone
             * */
            setReadOnly( a , 'setting' , setting );
            
            /**
             * @public
             * @readonly
             * @property {?(HTMLElement|Element|Node)} closest
             * @memberof EventClone
             * */
            setReadOnly( a , 'closest' , closest );
            
            /**
             * @public
             * @readonly
             * @property {HTMLElement|Element|Node} element
             * @memberof EventClone
             * */
            setReadOnly( a , 'element' , element );
            
            /**
             * @public
             * @readonly
             * @property {event} defaultEvent
             * @memberof EventClone
             * */
            setReadOnly( a , 'defaultEvent' , event );
            
            if ( isobject( override ) ) {
                for ( var i in override ) {
                    setReadOnly( a , i , override[ i ] );
                }
            }
            
            if ( setting.passive && !eventPassiveSupport ) {
                
                /**
                 * @public
                 * @readonly
                 * @function preventDefault
                 * @memberof EventClone
                 * */
                setReadOnly( a , 'preventDefault' , function () {
                    throw "Unable to preventDefault inside passive event listener invocation.";
                } );
            }
            
            return a;
        };
        
    } )();
    
    /** @class eventHandler */
    function eventHandler () {
        var self = this;
        
        /**
         * @public
         * @property {number} listened
         * @memberof eventHandler#
         * */
        this.listened = 0;
        
        /**
         * @public
         * @function closest
         * @memberof eventHandler#
         * */
        this.closest = closest;
        
        /**
         * @private
         * @property {number} guid
         * @memberof eventHandler#
         * */
        this.guid = 0;
        
        /**
         * @private
         * @property {document} document
         * @memberof eventHandler#
         * */
        this.document = document;
        
        /**
         * @private
         * @property {boolean} lightMemory
         * @memberof eventHandler#
         * */
        this.lightMemory = false;
        
        /**
         * @private
         * @property {?function} selfDestruction
         * @memberof eventHandler#
         * */
        this.selfDestruction = null;
        
        /**
         * @private
         * @property {boolean} selfDestructionEnabled
         * @memberof eventHandler#
         * */
        this.selfDestructionEnabled = false;
        
        /**
         * @private
         * @property {?number} selfDestructionTimeout
         * @memberof eventHandler#
         * */
        this.selfDestructionTimeout = null;
        
        /**
         * @private
         * @property {?number} cleanupInterval
         * @memberof eventHandler#
         * */
        this.cleanupInterval = null;
        
        /**
         * @private
         * @property {boolean} defaultPassive
         * @memberof eventHandler#
         * */
        this.defaultPassive = false;
        
        /**
         * @private
         * @property {Object} indexer
         * @memberof eventHandler#
         * */
        this.indexer = {};
        
        /**
         * @private
         * @property {Array} nodelist
         * @memberof eventHandler#
         * */
        this.nodelist = [];
        
        /**
         * @private
         * @property {?MutationObserver} observer
         * @memberof eventHandler#
         * */
        this.observer = null;
        
        /**
         * @private
         * @property {?number} observerTimeout
         * @memberof eventHandler#
         * */
        this.observerTimeout = null;
        
        /**
         * @private
         * @property {string} token
         * @memberof eventHandler#
         * */
        this.token = 'event-handler-guid-' + ( ++guid );
        
        /**
         * @private
         * @function handleEvent
         * @memberof eventHandler#
         * */
        this.handleEvent = function ( e ) {
            var i = 0 ,
                arg = [] ,
                remove = [] ,
                namespace = null ,
                token = self.token ,
                element = this.element ,
                closestElement , event , filters , clone , tmp , brk = false;
            
            if ( e.eventHandler ) {
                if ( token !== e.eventHandler.token ) {
                    return;
                }
                namespace = e.eventHandler.namespace;
                arg = e.eventHandler.arg;
            }
            
            for ( var callbacklist = this.callbacks.slice( 0 ) , l = callbacklist.length , data ; i < l ; i++ ) {
                if ( brk ) {
                    break;
                }
                
                data = callbacklist[ i ];
                
                // -----
                
                // check namespace match / blacklist and selector filter
                
                filters = {
                    
                    get namespace () {
                        return !namespace || compareNamespace( namespace , data.namespace );
                    } ,
                    
                    get blacklist () {
                        return !data.blacklist || !self.closest( e.target , data.blacklist );
                    } ,
                    
                    get selector () {
                        return !data.selector || ( closestElement = self.closest( e.target , data.selector ) );
                    }
                    
                };
                
                // -----
                
                if ( filters.selector && filters.namespace && filters.blacklist ) {
                    
                    clone = cloneSettings( data );
                    
                    // check autokill
                    if ( isfunction( data.autokill ) && data.autokill( clone ) === true ) {
                        remove.push( data );
                        continue;
                    }
                    
                    // clone event
                    /** @type {EventClone} */
                    event = cloneEvent( e , clone , closestElement || null , element , {
                        
                        /**
                         * @public
                         * @override
                         * @function stopImmediatePropagation
                         * @memberof event#
                         * */
                        stopImmediatePropagation : function () {
                            this.defaultEvent.stopImmediatePropagation();
                            brk = true;
                        } ,
                        
                        /**
                         * @public
                         * @override
                         * @function mute
                         * @memberof event#
                         * */
                        mute : function () {
                            !data.once && remove.push( data );
                        }
                        
                    } );
                    
                    // check once
                    if ( data.once ) {
                        remove.push( data );
                    }
                    
                    // callback
                    concat.apply( ( tmp = [ event ] , tmp ) , arg );
                    data.caller.apply( closestElement || element , tmp );
                }
            }
            
            // autokill / once / mute purge
            // -------------------------------------------------
            remove.length && self.muteCallbacks( remove );
        };
    }
    
    /**
     * @public
     * @function setDocument
     * @memberof eventHandler#
     * */
    eventHandler.prototype.setDocument = function ( d ) {
        this.document = d;
        this.setLightMemoryMode( this.lightMemory );
    };
    
    /**
     * @private
     * @function muteCallbacks
     * @memberof eventHandler#
     * */
    eventHandler.prototype.muteCallbacks = function ( callbacks ) {
        var self = this ,
            handler , handlers ,
            element , callList , index;
        
        foreach( toarray( callbacks ) , function ( callback ) {
            handler = callback.referer;
            handlers = handler.referer;
            element = handler.element;
            callList = handler.callbacks;
            
            if ( callList.length ) {
                index = callList.indexOf( callback );
                
                if ( index > -1 ) {
                    callList.splice( callList.indexOf( callback ) , 1 );
                    self.listened--;
                }
            }
            
            if ( !callList.length ) {
                element.removeEventListener( handler.event , handler , handler.capture );
                delete handlers[ handler.name ];
            }
            
            if ( isEmptyObject( handlers ) ) {
                self.deleteElement( element );
            }
        } );
    };
    
    /**
     * @private
     * @function cleanup
     * @memberof eventHandler#
     * */
    eventHandler.prototype.cleanup = ( function () {
        var remove = [];
        
        function browseCallbacks ( handleObject ) {
            var callbacks = handleObject.callbacks;
            
            for ( var i = 0 , l = callbacks.length , callback ; i < l ; i++ ) {
                callback = callbacks[ i ];
                if ( callback.autokill && callback.autokill( handleObject ) === true ) {
                    remove.push( callback );
                }
            }
        }
        
        function browsehandlers ( handler ) {
            for ( var h in handler ) {
                browseCallbacks( handler[ h ] );
            }
            
            if ( remove.length ) {
                this.muteCallbacks( remove );
            }
            
            remove = [];
        }
        
        return function () {
            var i , x , tmp;
            
            if ( isEmptyObject( this.indexer ) ) {
                return this.checkSelfDestruction();
            }
            
            for ( i in x = this.indexer ) {
                tmp = x[ i ];
                browsehandlers.call( this , tmp.handlers );
            }
        };
    } )();
    
    /**
     * @public
     * @function setCleanupInterval
     * @memberof eventHandler#
     * */
    eventHandler.prototype.setCleanupInterval = function ( n ) {
        var self = this;
        clearInterval( this.cleanupInterval );
        if ( exist( n ) && Number.isInteger( n ) ) {
            this.cleanupInterval = setInterval( function () { self.cleanup.call( self ); } , n );
        }
    };
    
    /**
     * @public
     * @function setDefaultPassive
     * @memberof eventHandler#
     * */
    eventHandler.prototype.setDefaultPassive = function ( b ) {
        this.defaultPassive = !!b;
    };
    
    /**
     * @public
     * @function setLightMemoryMode
     * @memberof eventHandler#
     * */
    eventHandler.prototype.setLightMemoryMode = function ( b ) {
        this.lightMemory = !!b;
        
        this.disableObserver();
        
        if ( this.lightMemory ) {
            this.enableObserver();
        }
    };
    
    /**
     * @public
     * @function setSelfDestruction
     * @memberof eventHandler#
     * */
    eventHandler.prototype.setSelfDestruction = function ( fn ) {
        this.selfDestruction = isfunction( fn ) ? fn : null;
        this.selfDestructionEnabled = !!this.selfDestruction;
        this.checkSelfDestruction();
    };
    
    // -----------------------------------
    // -----------------------------------
    
    /**
     * @private
     * @function setPassive
     * @memberof eventHandler#
     * */
    eventHandler.prototype.setPassive = function ( b ) {
        return ( exist( b ) && typeof ( b ) == 'boolean' ) ? b : this.defaultPassive;
    };
    
    /**
     * @private
     * @function observerCallback
     * @memberof eventHandler#
     * */
    eventHandler.prototype.observerCallback = function () {
        clearTimeout( this.observerTimeout );
        this.observerTimeout = setTimeout( function () {
            var d = this.document;
            
            for ( var a = this.nodelist , i = 0 , t ; i < a.length ; i++ ) {
                t = a[ i ];
                
                if ( t.nodeType != 1 ) {
                    continue;
                }
                
                if ( !d.contains( t ) ) {
                    this.mute( t );
                    i--;
                }
            }
            
            if ( !this.nodelist.length ) {
                this.disableObserver();
            }
        }.bind( this ) , 100 );
    };
    
    /**
     * @private
     * @function enableObserver
     * @memberof eventHandler#
     * */
    eventHandler.prototype.enableObserver = function () {
        if ( !this.lightMemory || this.observer ) {
            return;
        }
        
        var mutation = global.MutationObserver || global.WebKitMutationObserver || null;
        
        if ( mutation ) {
            this.observer = new mutation( this.observerCallback.bind( this ) );
            this.observer.observe( this.document , { childList : true , subtree : true } );
            this.observerCallback();
        }
    };
    
    /**
     * @private
     * @function disableObserver
     * @memberof eventHandler#
     * */
    eventHandler.prototype.disableObserver = function () {
        clearTimeout( this.observerTimeout );
        if ( this.observer ) {
            this.observer.disconnect();
            this.observer = null;
        }
    };
    
    /**
     * @private
     * @function spliceNodelist
     * @memberof eventHandler#
     * */
    eventHandler.prototype.spliceNodelist = function ( e ) {
        var i = this.nodelist.indexOf( e );
        i >= 0 && this.nodelist.splice( i , 1 );
    };
    
    /**
     * @private
     * @function checkSelfDestruction
     * @memberof eventHandler#
     * */
    eventHandler.prototype.checkSelfDestruction = function () {
        clearTimeout( this.selfDestructionTimeout );
        if ( this.selfDestructionEnabled ) {
            this.selfDestructionTimeout = setTimeout( function () {
                if ( !this.nodelist.length && isfunction( this.selfDestruction ) && this.selfDestruction( this ) === true ) {
                    this.destroy();
                }
            }.bind( this ) , 100 );
        }
    };
    
    /**
     * @private
     * @function parseObject
     * @memberof eventHandler#
     * */
    eventHandler.prototype.parseObject = function ( element , json , selector , capture ) {
        var i;
        
        if ( !selector ) {
            for ( i in json ) {
                this.listen( element , i , json[ i ] , capture );
            }
        }
        else {
            for ( i in json ) {
                this.listen( element , i , selector , json[ i ] , capture );
            }
        }
    };
    
    /**
     * @private
     * @function validObjectArgument
     * @memberof eventHandler#
     * */
    eventHandler.prototype.validObjectArgument = function ( args , object ) {
        isboolean( object.once ) && ( args.once = !!object.once );
        isboolean( object.capture ) && ( args.capture = !!object.capture );
        args.passive = this.setPassive( object.passive );
        
        isstring( object.selector ) && ( args.selector = object.selector );
        isstring( object.blacklist ) && ( args.blacklist = object.blacklist );
        
        args.autokill = isfunction( object.autokill ) && object.autokill || null;
        args.namespace = parseNamespace( object.namespace );
    };
    
    /**
     * @private
     * @function parseArguments
     * @memberof eventHandler#
     * */
    eventHandler.prototype.parseArguments = function ( element , event , selector , callback , capture ) {
        
        if ( isobject( event ) ) {
            return this.parseObject( element , event , null , selector );
        }
        
        if ( isobject( selector ) ) {
            return this.parseObject( element , selector , event , callback );
        }
        
        if ( !element[ this.token ] ) {
            element[ this.token ] = ++this.guid;
            
            this.nodelist.push( element );
            
            this.indexer[ element[ this.token ] ] = {
                element : element ,
                handlers : {}
            };
        }
        
        var args = {
            callback : callback ,
            
            passive : this.defaultPassive ,
            capture : isboolean( capture ) && capture ,
            once : false ,
            
            selector : selector ,
            blacklist : null ,
            
            namespace : null ,
            autokill : null
        };
        
        if ( isfunction( selector ) ) {
            args.capture = isboolean( callback ) && !!callback;
            args.callback = selector;
            args.selector = null;
            
            if ( isobject( callback ) ) {
                this.validObjectArgument( args , callback );
            }
        }
        
        if ( isobject( capture ) ) {
            this.validObjectArgument( args , capture );
        }
        
        args.events = buildEvents( event , args.namespace );
        
        if ( !args.callback[ this.token ] ) {
            args.callback[ this.token ] = ++this.guid;
        }
        
        return args;
    };
    
    /**
     * @private
     * @function findHandlers
     * @memberof eventHandler#
     * */
    eventHandler.prototype.findHandlers = function ( element , events , checkNamespace ) {
        var self = this ,
            eventsList , _token;
        
        function createEventsList () {
            eventsList = [];
            
            ( events = ( isarray( events ) ? events : buildEvents( events ) ) ).forEach( function ( model ) {
                if ( model.event ) {
                    eventsList.push( model.event );
                }
            } );
        }
        
        function loop ( condition ) {
            var r = [] , index = {} ,
                element , token , rih , rc , ro;
            
            forin( self.indexer , function ( key , indexer ) {
                element = indexer.element;
                token = element[ self.token ];
                
                if ( token ) {
                    forin( indexer.handlers , function ( key , handler ) {
                        
                        if ( condition( token , handler.event ) ) {
                            
                            if ( !index[ token ] ) {
                                ro = {
                                    element : element ,
                                    handlers : {} ,
                                    callbacks : []
                                };
                                
                                index[ token ] = r.push( ro );
                                
                                rih = ro.handlers;
                                rc = ro.callbacks;
                            }
                            
                            rih[ key ] = handler;
                            
                            if ( checkNamespace ) {
                                concat.apply( rc , filterByNamespace( handler.event , handler.callbacks ) );
                            }
                        }
                        
                    } );
                }
            } );
            
            return r;
        }
        
        function filterByNamespace ( event , callbacks ) {
            var r = [];
            
            foreach( events , function ( model ) {
                if ( !model.event || model.event == event ) {
                    for ( var i = 0 , l = callbacks.length , tmp ; i < l ; i++ ) {
                        tmp = callbacks[ i ];
                        
                        if ( !model.namespace || !tmp.namespace || compareNamespace( model.namespace , tmp.namespace ) ) {
                            r.push( tmp );
                        }
                    }
                }
            } );
            
            return r;
        }
        
        if ( element && events ) {
            if ( !( _token = element[ self.token ] ) ) {
                return [];
            }
            
            createEventsList();
            
            return loop( function ( token , event ) {
                return _token == token && ( !eventsList.length || inarray( event , eventsList ) );
            } );
        }
        
        if ( events ) {
            createEventsList();
            
            return loop( function ( token , event ) {
                return !eventsList.length || inarray( event , eventsList );
            } );
        }
        
        if ( element ) {
            if ( !( _token = element[ self.token ] ) ) {
                return [];
            }
            
            return ( function () {
                var r = [] ,
                    handlers , element , token;
                
                forin( self.indexer , function ( key , indexer ) {
                    element = indexer.element;
                    handlers = indexer.handlers;
                    token = element[ self.token ];
                    
                    if ( token && _token == token ) {
                        r.push( {
                            element : element ,
                            handlers : handlers
                        } );
                    }
                } );
                
                return r;
            } )();
        }
        
        return [];
    };
    
    /**
     * @public
     * @function listen
     * @memberof eventHandler#
     * */
    eventHandler.prototype.listen = function ( element , events , selector , callback , capture ) {
        var data , arg , tmp , eventName , nameKey , handler;
        
        if ( !( arg = this.parseArguments( element , events , selector , callback , capture ) ) ) {
            return;
        }
        
        data = this.indexer[ element[ this.token ] ];
        
        nameKey = ( +arg.capture ) /*+ (+arg.passive)*/;
        
        events = arg.events;
        
        for ( var i = 0 , l = events.length ; i < l ; ) {
            tmp = events[ i++ ];
            eventName = tmp.event + nameKey;
            handler = data.handlers[ eventName ];
            
            if ( !handler ) {
                element.addEventListener(
                    tmp.event ,
                    
                    data.handlers[ eventName ] = handler = {
                        name : eventName ,
                        callbacks : [] ,
                        element : element ,
                        event : tmp.event ,
                        capture : arg.capture ,
                        referer : data.handlers ,
                        handleEvent : this.handleEvent
                    } ,
                    
                    arg.passive && eventPassiveSupport ? {
                            capture : arg.capture ,
                            passive : arg.passive
                        } :
                        arg.capture
                );
            }
            
            handler.callbacks.push( {
                referer : handler ,
                
                caller : arg.callback ,
                guid : arg.callback[ this.token ] ,
                
                once : arg.once ,
                passive : arg.passive ,
                capture : arg.capture ,
                
                selector : arg.selector ,
                blacklist : arg.blacklist ,
                
                autokill : arg.autokill ,
                namespace : tmp.namespace
            } );
            
            this.listened++;
        }
        
        this.enableObserver();
    };
    
    /**
     * @public
     * @function localDispatch
     * @memberof eventHandler#
     * */
    eventHandler.prototype.localDispatch = function ( element , events , arg ) {
        var self = this;
        
        function ready ( result ) {
            var remove = [] ,
                getNamespaceArgument , element ,
                namespaces , fakeevent , clone , fakeclone , tmp;
            
            foreach( result , function ( data ) {
                element = data.element;
                getNamespaceArgument = sortNamespace( events );
                
                // for each callback
                foreach( data.callbacks , function ( callback ) {
                    
                    // check namespace
                    if ( events ) {
                        namespaces = getNamespaceArgument( callback.referer.event );
                        
                        if ( namespaces.length && !compareNamespace( namespaces , callback.namespace ) ) {
                            return;
                        }
                    }
                    
                    clone = cloneSettings( callback );
                    
                    // check autokill
                    if ( isfunction( callback.autokill ) && callback.autokill( clone ) === true ) {
                        return remove.push( callback );
                    }
                    
                    fakeevent = createEventObject( getdocument( element ) , callback.referer.event );
                    
                    /** @type {EventClone} */
                    fakeclone = cloneEvent( fakeevent , cloneSettings( callback ) , null , element , {
                        originalTarget : element ,
                        currentTarget : element ,
                        srcElement : element ,
                        target : element ,
                        
                        /**
                         * @public
                         * @override
                         * @function mute
                         * @memberof fakeclone#
                         * */
                        mute : function () {
                            !callback.once && remove.push( callback );
                        }
                    } );
                    
                    // check once
                    if ( callback.once ) {
                        remove.push( callback );
                    }
                    
                    concat.apply( ( tmp = [ fakeclone ] , tmp ) , toarray( arg ) );
                    
                    callback.caller.apply( element , tmp );
                } );
            } );
            
            // autokill / once / mute purge
            if ( remove.length ) {
                self.muteCallbacks( remove );
            }
        }
        
        var result = ( function () {
            
            if ( exist( element ) && isstring( events ) ) {
                events = buildEvents( events );
                return self.findHandlers( element , events , true );
            }
            
            if ( isstring( element ) ) {
                arg = events;
                events = buildEvents( element );
                return self.findHandlers( null , events , true );
            }
            
            if ( exist( element ) ) {
                arg = events;
                events = null;
                return self.findHandlers( element , null );
            }
            
        } )();
        
        if ( result && result.length ) {
            return ready( result );
        }
        
    };
    
    /**
     * @public
     * @function dispatch
     * @memberof eventHandler#
     * */
    eventHandler.prototype.dispatch = function ( element , events , arg , forceEmulate ) {
        var emulate = false , self = this;
        
        if ( exist( element ) && isstring( events ) ) {
            arg = toarray( arg );
            events = buildEvents( events );
            emulate = !!( arg.length || forceEmulate );
            
            events.forEach( function ( model ) {
                if ( model.event ) {
                    dispatch( element , model.event , {
                        namespace : model.namespace ,
                        token : self.token ,
                        arg : arg
                    } , emulate );
                }
            } );
        }
        
    };
    
    /**
     * @public
     * @function mute
     * @memberof eventHandler#
     * */
    eventHandler.prototype.mute = function ( element , events , fn ) {
        var guid , self = this;
        
        // mute all
        if ( !exist( element ) && !exist( events ) ) {
            return this.bigCrunch();
        }
        
        if ( isfunction( events ) ) {
            fn = events;
        }
        
        // invalid function
        if ( isfunction( fn ) && !( guid = fn[ this.token ] ) ) {
            return;
        }
        
        function ready ( result ) {
            var callbacks , callback ,
                namespaces , element ,
                getNamespaceArgument = sortNamespace( events );
            
            // browse all handleEvent that match
            foreach( result , function ( data ) {
                element = data.element;
                
                // for each handlers
                forin( data.handlers , function ( key , handler ) {
                    
                    // filter by namespace or function
                    if ( events || guid ) {
                        callbacks = handler.callbacks;
                        
                        for ( var i = 0 , l = callbacks.length ; i < l ; i++ ) {
                            callback = callbacks[ i ];
                            
                            // filter by namespace
                            if ( events ) {
                                namespaces = getNamespaceArgument( handler.event );
                                
                                if ( namespaces.length && !compareNamespace( namespaces , callback.namespace ) ) {
                                    continue;
                                }
                            }
                            
                            // filter by function
                            if ( guid && callback.guid !== guid ) {
                                continue;
                            }
                            
                            callbacks.splice( i , 1 );
                            self.listened--;
                            i--;
                            l--;
                        }
                    }
                    // or just filter by event
                    else {
                        self.listened -= handler.callbacks.length;
                        handler.callbacks = [];
                    }
                    
                    // if haven't any callback, remove the event and delete the object
                    if ( !handler.callbacks.length ) {
                        element.removeEventListener( handler.event , handler , handler.capture );
                        delete handler.referer[ handler.name ];
                    }
                } );
                
                // if havn't any events for the element, purge it
                if ( isEmptyObject( self.indexer[ element[ self.token ] ].handlers ) ) {
                    self.deleteElement( data.element );
                }
                
            } );
            
        }
        
        var result = ( function () {
            
            if ( exist( element ) && isstring( events ) ) {
                events = buildEvents( events );
                return self.findHandlers( element , events );
            }
            
            if ( isstring( element ) ) {
                events = buildEvents( element );
                return self.findHandlers( null , events );
            }
            
            if ( exist( element ) ) {
                events = null;
                return self.findHandlers( element , null );
            }
            
        } )();
        
        if ( result && result.length ) {
            return ready( result );
        }
        
    };
    
    /**
     * @public
     * @function muteChild
     * @memberof eventHandler#
     * */
    eventHandler.prototype.muteChild = function ( parent , event , callback ) {
        this.mute( parent , event , callback );
        
        for ( var a = getallchildren( parent ) , i = 0 , l = a.length ; i < l ; i++ ) {
            this.mute( a[ i ] , event , callback );
        }
    };
    
    /**
     * @private
     * @function deleteElement
     * @memberof eventHandler#
     * */
    eventHandler.prototype.deleteElement = function ( e ) {
        delete this.indexer[ e[ this.token ] ];
        delete e[ this.token ];
        this.spliceNodelist( e );
        this.checkSelfDestruction();
    };
    
    /**
     * @private
     * @function bigCrunch
     * @memberof eventHandler#
     * */
    eventHandler.prototype.bigCrunch = function () {
        var self = this , handler , i;
        
        forin( this.indexer , function ( key , tmp ) {
            for ( i in tmp.handlers ) {
                handler = tmp.handlers[ i ];
                tmp.element.removeEventListener( handler.event , handler , handler.capture );
                self.listened -= handler.callbacks.length;
            }
            
            delete tmp.element[ self.token ];
        } );
        
        this.guid = 0;
        this.indexer = {};
        this.nodelist = [];
        return this.checkSelfDestruction();
    };
    
    /**
     * @public
     * @function destroy
     * @memberof eventHandler#
     * */
    eventHandler.prototype.destroy = function () {
        clearInterval( this.cleanupInterval );
        delete this.cleanupInterval;
        
        clearTimeout( this.selfDestructionTimeout );
        delete this.selfDestructionEnabled;
        delete this.selfDestructionTimeout;
        delete this.selfDestruction;
        
        this.disableObserver();
        delete this.observerTimeout;
        delete this.lightMemory;
        delete this.observer;
        
        this.bigCrunch();
        
        delete this.defaultPassive;
        delete this.handleEvent;
        delete this.document;
        delete this.nodelist;
        delete this.indexer;
        delete this.token;
        delete this.guid;
    };
    
    /**
     * @public
     * @property {Array} eventHandler.shortcuts
     * */
    ( eventHandler.shortcuts = [
        'click' , 'dblclick' , 'contextmenu' ,
        'mouseup' , 'mousedown' , 'mousemove' , 'mouseover' , 'mouseout' ,
        'blur' , 'focus' , 'change' , 'input' , 'submit' ,
        'keydown' , 'keypress' , 'keyup' ,
        'touchstart' , 'touchmove' , 'touchend' , 'touchcancel' ,
        'dragstart' , 'dragenter' , 'dragover' , 'dragleave' , 'drop' ,
        'error' , 'scroll' , 'load' , 'resize'
    ] ).forEach( function ( name ) {
        eventHandler.prototype[ name ] = function ( element , selector , callback , capture ) {
            if ( arguments.length <= 3 && !isfunction( selector ) && !isfunction( callback ) ) {
                return this.dispatch( element , name , selector , callback );
            }
            
            this.listen( element , name , selector , callback , capture );
        };
    } );
    
    /**
     * @public
     * @function eventHandler.redirect
     * */
    eventHandler.redirect = function ( element , e ) {
        var oldData;
        
        e.stopImmediatePropagation();
        
        if ( e.defaultEvent ) {
            e = e.defaultEvent;
        }
        
        if ( e.originalEvent ) {
            e = e.originalEvent;
        }
        
        oldData = e.eventHandler || null;
        
        e = new e.constructor( e.type , e );
        
        if ( oldData ) {
            e.eventHandler = oldData;
        }
        
        dispatch( element , e );
    };
    
    /**
     * @public
     * @readonly
     * @function eventHandler
     * @return eventHandler#
     * */
    _defineProperty( global , 'eventHandler' , eventHandler );
    
} )();